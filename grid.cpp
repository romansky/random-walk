#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

int main(int argc, char** argv)
{	
	if (argc < 3) {
		cout << "Not enough arguments!" << endl;
	}

	int steps = stoi(argv[1]);
	int walks = stoi(argv[2]);

	int x = 0;
	int y = 0;

	srand(time(nullptr));
	int xcoin;
	int ycoin;

	ofstream outFile;
	outFile.open("2DWalk.txt");
	
	for (int i=0; i < walks; i++)
	{
		for (int j=0; j < steps; j++)
		{
			xcoin = 1 + rand() % 2;
			ycoin = 1 +rand() % 2;

			if (xcoin == 1 /*heads*/) {
				x++;
			} else if (xcoin == 2 /*tails*/) {
				x--;
			}

			if (ycoin == 1 /*heads*/) {
				y++;
			} else if (ycoin == 2 /*tails*/) {
				y--;
			}
		}

		cout << "Starting Position: (0,0)" << endl;
		cout << "Ending Position: (" << x << "," << y << ")" << endl;
		cout << "Number of Steps: " << steps << endl;

		outFile << x << " " << y << endl;
	}


	return 0;
}
