.PHONY: all

GCC = g++
CFLAGS = -std=c++11 -Wall -O3
LDFLAGS = -I.
OBJDIR = obj

CLASSES = graph randomWalk
OBJECTS = $(addsuffix .o, $(CLASSES))
OBJFILES = $(addprefix $(OBJDIR)/, $(OBJECTS))

all: connectedGraph grid numberLine

connectedGraph: $(OBJDIR)/connectedGraph.o $(OBJFILES)
	$(GCC) -o connectedGraph $(CFLAGS) $(OBJDIR)/connectedGraph.o $(OBJFILES) $(LDFLAGS)

grid: $(OBJDIR)/grid.o $(OBJFILES)
	$(GCC) -o grid $(CFLAGS) $(OBJDIR)/grid.o $(OBJFILES) $(LDFLAGS)

$(OBJDIR)/connectedGraph.o: connectedGraph.cpp
	$(GCC) -c $(CFLAGS) connectedGraph.cpp -o $(OBJDIR)/connectedGraph.o

$(OBJDIR)/grid.o: grid.cpp
	$(GCC) -c $(CFLAGS) grid.cpp -o $(OBJDIR)/grid.o

.SECONDEXPANSION:
$(OBJFILES): %.o: $$(notdir %.cpp %.h)
	$(GCC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(OBJDIR)/*.o
	rm numberLine grid connectedGraph
	rm *.txt
