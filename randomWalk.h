#ifndef RANDOMWALK_H
#define RANDOMWALK_H
#include "graph.h"

void random_walk(int* numberLine);
void random_walk(int** grid);
void random_walk(graph g);

#endif
