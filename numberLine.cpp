#include <cstdlib>
#include <iostream>
#include <string>
#include <ctime>
#include <fstream>

using namespace std;

int main(int argc, char** argv)
{

	if (argc < 3) {
		cout << "No argument passed!" << endl;
		return 1;
	}

	int walks = stoi(argv[2]);

	int steps = stoi(argv[1]);

	int walker = 0;

	int coin; 

	ofstream outFile;
	outFile.open("1DWalk.txt");

	srand(time(nullptr));

	for (int i=0; i < walks; i++)
	{
		for (int j=0; j < steps; j++)
		{
			coin  = 1 + rand() % 2;
			if (coin == 1 /*heads*/) {
				walker++;
			} else if (coin == 2 /*tails*/) {
				walker--;
			}
		}

		cout << "Starting Position: 0" << endl;
		cout << "Ending Position: " << walker << endl;
		cout << "Number of steps: " << steps << endl;

		outFile << walker << endl;

	}
	return 0;
}
